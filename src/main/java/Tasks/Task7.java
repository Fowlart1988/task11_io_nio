package Tasks;

import Utils.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.channels.FileChannel;

public class Task7 {
    public static void main(String[] args) {
        try {
            File f = new File("art.dat");
            f.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(f);
            SomeBuffer someBuffer = new SomeBuffer("Some big data");
            FileChannel channel = fileOutputStream.getChannel();
            int out;
            while ((out = someBuffer.next()) != -1) {
                System.out.print((char) out + " ");
            }
            channel.write(someBuffer.getBuffer());
            channel.close();

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
