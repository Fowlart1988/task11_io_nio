
package IoNioStudy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ReaderWriterLibUseExamlple {

    /**
     * Суть прикладу: беремо клас із новішої лібки OutputStreamWriter, пишемо в нього
     * стрінгу розкладаємо її на байти та записуємо в ByteArrayOutputStream(стара лібка).
     * А потім коректно читаємо кожен байт з масиву переводячи його в символ.
     */

    public static void main(String[] args) throws IOException {

// ByteArrayOutputStream - може зберігати дані в порядку їх додавання.
// З цього можна отримати масив байтів.
        ByteArrayOutputStream storage = new ByteArrayOutputStream();

/** OutputStreamWriter клас із новішої лібки WRITER, який здатний писати тільки символи або строки*/
        OutputStreamWriter buf = new OutputStreamWriter(storage, "UTF-8");
        buf.append("JAV");
        buf.flush();
        buf.close();
        DataInputStream streamIn = new DataInputStream(new ByteArrayInputStream(storage.toByteArray()));
        System.out.println((char) streamIn.readByte());
        System.out.println((char) streamIn.readByte());
        System.out.println((char) streamIn.readByte());
        streamIn.close();
    }

}
