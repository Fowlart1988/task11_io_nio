
package IoNioStudy;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class PrimiteveTransactions {

    public static void main(String[] args) throws IOException {


/** ByteArrayOutputStream - може зберігати дані в порядку їх додавання.
/* З цього можна отримати масив байтів. */
        ByteArrayOutputStream storage = new ByteArrayOutputStream();

// Наробили декораторів; записали примітиви в певній послідовності.
        DataOutputStream stream = new DataOutputStream(new BufferedOutputStream(storage));
        stream.writeDouble(24);
        stream.writeInt(28);
        stream.writeInt(60);
        stream.flush();
        stream.close();
        storage.close();

// Прочитали їх в тій самій послідовності.
// Щоб коректно прочитати примітиви - важливо знати порядок їх розміщення. Інакше можна вихопити exception.
        DataInputStream streamIn = new DataInputStream(new BufferedInputStream(new ByteArrayInputStream(storage.toByteArray())));
        System.out.println(streamIn.readDouble());
        System.out.println(streamIn.readInt());
        System.out.println(streamIn.readInt());
        streamIn.close();

    }
}
