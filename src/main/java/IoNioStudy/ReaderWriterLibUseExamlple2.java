package IoNioStudy;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Спроба удосконалити приклад - читати кирилицю із масиву байтів
 */
public class ReaderWriterLibUseExamlple2 {


    public static void main(String[] args) throws IOException {

        ByteArrayOutputStream storage = new ByteArrayOutputStream();

/** OutputStreamWriter клас із новішої лібки WRITER, який здатний писати тільки символи або строки*/
        OutputStreamWriter buf = new OutputStreamWriter(storage);
        buf.append("JAVA IS THE BEST");
        buf.flush();
        buf.close();
        BufferedReader streamIn =
                // клас із новішої бібліотеки
                new BufferedReader(
                        // клас адаптер для переводу із старіших ліб до новіших
                        new InputStreamReader(
                                // буферизація із старіших класів
                                new BufferedInputStream(
                                        // читаємо із масиву байтів
                                        new ByteArrayInputStream(storage.toByteArray()))));
        System.out.println(streamIn.readLine());
        streamIn.close();
    }
}
