package Utils;

import java.io.*;
import java.util.LinkedList;
import java.util.List;


public class BufferedInputFile {

    public static List<String> commentslist =new LinkedList<>();

    public static String read(String filename) throws IOException {

        // Reading input by lines:
        BufferedReader in = new BufferedReader(new FileReader(filename));
        StringBuilder sb = new StringBuilder();
String s;
        // Доводится додавати символи переходу на нову стрічку вручну
        while((s = in.readLine())!=null){
            sb.append(s+"\n");
            if (s.contains("//")) commentslist.add(s);

        }
        in.close();
        return sb.toString();
    }
}
