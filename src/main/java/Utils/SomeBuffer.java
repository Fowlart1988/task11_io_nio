package Utils;

import java.nio.*;

public class SomeBuffer {

    private ByteBuffer buffer;

    public SomeBuffer(String data) {
        // single character in String is decode into char type,
        // every char is 2 byte, so data.length()*2 might be enough
        this.buffer = ByteBuffer.allocate(data.length()*2);
        buffer.asCharBuffer().put(data);
        buffer.rewind();
    }

    public int next() {
      if (buffer.hasRemaining()) return buffer.getChar();
      else return -1;
    }

    public ByteBuffer getBuffer() {
        buffer.rewind();
       // buffer.flip();
        return buffer;
    }
}
