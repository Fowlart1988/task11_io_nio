package Utils;
// Copying a file using channels and buffers
import java.nio.*;
import java.nio.channels.*;
import java.io.*;
import java.time.LocalDateTime;

public class ChannelCopy {
    public static void copy (String inFile, String outFile, int bsize) throws Exception {

        FileChannel in = new FileInputStream(inFile).getChannel(), out = new FileOutputStream(outFile).getChannel();
        ByteBuffer buffer;
        if (bsize<=0) buffer = ByteBuffer.allocateDirect(1024);
        else  buffer = ByteBuffer.allocate(bsize);
        LocalDateTime clock = LocalDateTime.now();
        System.out.println(clock.toLocalTime());
        while (in.read(buffer) != -1) {
            buffer.flip(); // Prepare for writing
            out.write(buffer);
            buffer.clear();  // Prepare for reading
        }
        clock = LocalDateTime.now();
        System.out.println(clock.toLocalTime());
    }
} 