package Utils;

public class OSExecuteException extends RuntimeException {
  public OSExecuteException(String why) { super(why); }
}
